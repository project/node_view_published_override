# Introduction
This module override View published content provided by core.
The following permissions will be added to each content type:
view any published content
view own published content

# Configuration

 1. Download and enable module.
 2. Check View published content provided by core.
 3. Configure your custom permissions under Node view published override.
 4. Done.

 ## Contact details
 Anas Mawlawi
 anas.mawlawi89@gmail.com
